﻿using DemoMVC.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMVC.Domain.Model
{
    public class Cheque
    {
        public custom_cheque ChequeDetail { get; set; }
        public string ChequeText { get; set; }
    }
}
