﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoMVC.Domain.Data;

namespace DemoMVC.Domain.Repository
{
    public class ChequeRepository : IChequeRepository
    {
        IKenticoDB _entities;

        public ChequeRepository(IKenticoDB entity)
        {
            _entities = entity;
        }   
             
        public IList<custom_cheque> GetCheques()
        {
            return (from cheque in _entities.Query<custom_cheque>()
                    orderby cheque.ChequeDate descending
                    select cheque).ToList();
        }

        public custom_cheque GetCheque(int chequeId)
        {
            return (from cheque in _entities.Query<custom_cheque>()
                    where cheque.ChequeID == chequeId
                    orderby cheque.ChequeDate descending
                    select cheque).FirstOrDefault();
        }
        
    }
}
