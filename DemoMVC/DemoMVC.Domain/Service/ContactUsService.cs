﻿using DemoMVC.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMVC.Domain.Service
{
    public class ContactUsService
    {
        Kentico10AppEntities _entities;

        public ContactUsService()
        {
            _entities = new Kentico10AppEntities();
        }

        public void SubmitContactUs(Form_DemoSite_ContactUs formData)
        {
            _entities.Form_DemoSite_ContactUs.Add(formData);
            _entities.SaveChanges();
        }
    }
}
