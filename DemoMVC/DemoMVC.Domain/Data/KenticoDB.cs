﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMVC.Domain.Data
{
    public class KenticoDB : DbContext, IKenticoDB
    {
        public KenticoDB() : base("name=Kentico10AppEntities")
        {

        }

        public DbSet<custom_cheque> Cheques { get; set; }

        IQueryable<T> IKenticoDB.Query<T>()
        {
            return Set<T>();

        }

        void IKenticoDB.Insert<T>(T formData)
        {
            DbSet dbSet = Set(formData.GetType());
            dbSet.Add(formData);
            SaveChanges();
        }



    }
}
