﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMVC.Filters
{
    public class CustomActionFilterAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            filterContext.Controller.ViewBag.NewData = filterContext.Controller.ControllerContext.RouteData.Values["controller"];
            Console.Write(filterContext.Controller.ControllerContext.RouteData.Values["controller"]);
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //throw new NotImplementedException();
        }
    }
}