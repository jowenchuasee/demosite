﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoMVC.Domain.Data;
using DemoMVC.Domain.Repository;
using DemoSite.Library.Services;
using DemoSite.Library.Helpers;
using DemoSite.Library.Dependency;

namespace DemoMVC.Controllers
{
    public class ChequesController : Controller
    {
        IKenticoDB _entities;

        public ChequesController()
        {
            _entities = new KenticoDB();
        }

        public ChequesController(IKenticoDB entities)
        {
            _entities = entities;
        }

        // GET: Cheques
        public ActionResult Index()
        {
            IChequeRepository repo = new ChequeRepository(_entities);
            var cheques = repo.GetCheques(); 
            return View(cheques);
        }

        public ActionResult Detail(int id)
        {
            DependencyServiceFactory<ChequeService> factory = new DependencyServiceFactory<ChequeService>();

            var cheque = factory.GetService().GetChequeDetail(id);
            return View(cheque);
        }

        [ChildActionOnly]
        public ActionResult Top3()
        {
            IChequeRepository repo = new ChequeRepository(_entities);

            var cheques = repo.GetCheques();

            var top3Cheques = (from c in cheques
                               orderby c.ChequeAmount descending
                               select c).Take(3).ToList();

            return PartialView("_Top3", top3Cheques);
        }
    }
}