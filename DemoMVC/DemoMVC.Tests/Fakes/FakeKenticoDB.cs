﻿using DemoMVC.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMVC.Tests.Fakes
{
    public class FakeKenticoDB : IKenticoDB
    {
        public IQueryable<T> Query<T>() where T:class
        {
            return Sets[typeof(T)] as IQueryable<T>;
        }

        public void Insert<T>(T formData) where T: class
        {
            Sets.Add(typeof(T), formData);
        }

        public void Dispose() { }

        public void AddSet<T>(IQueryable<T> objects)
        {
            Sets.Add(typeof(T), objects);
            
        }

        public Dictionary<Type, object> Sets = new Dictionary<Type, object>();
    }
}
