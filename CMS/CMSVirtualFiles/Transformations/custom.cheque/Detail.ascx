﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.DocumentEngine.Web.UI.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.DocumentEngine.Web.UI" Assembly="CMS.DocumentEngine.Web.UI" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.DocumentEngine.Web.UI" Assembly="CMS.DocumentEngine.Web.UI" %><%@ Import  Namespace="DemoSite.Library.Helpers" %>
<table class="table">
  <tr>
    <th>Name</th>
    <td><%#Eval("ChequeName")%></td>
  </tr>
  <tr>
    <th>Date</th>
    <td><%#FormatDateTime(Eval("ChequeDate"), "dd/MM/yyyy")%></td>
  </tr>
  <tr>
    <th>Amount</th>
    <td>$<%#Convert.ToDouble(Eval("ChequeAmount")).ToString("#,##0.00")%></td>
  </tr>
  <tr>
    <th>Pay</th>
    <td><%#TextConverterHelper.ConvertToDollarText(Convert.ToDouble(Eval("ChequeAmount"))) %></td>
  </tr>
</table>
<a href="~/Cheques">Back to List</a>