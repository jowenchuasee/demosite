﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.DocumentEngine.Web.UI.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.DocumentEngine.Web.UI" Assembly="CMS.DocumentEngine.Web.UI" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.DocumentEngine.Web.UI" Assembly="CMS.DocumentEngine.Web.UI" %><tr>
  <td><a href="<%# GetUrl(Eval("NodeAliasPath"), Eval("DocumentUrlPath")) %>"><%#Eval("ChequeName")%></a></td>
  <td>$<%#Convert.ToDouble(Eval("ChequeAmount")).ToString("#,##0.00")%></td>
  <td><%#FormatDateTime(Eval("ChequeDate"), "dd/MM/yyyy")%></td>
</tr>