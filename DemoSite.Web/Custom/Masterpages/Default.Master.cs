﻿using CMS.UIControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoSite.Web.Custom.Masterpages
{
    public partial class Default : TemplateMasterPage
    {
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            PageManager = CMSPortalManager1;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.ltlTags.Text = this.HeaderTags;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}