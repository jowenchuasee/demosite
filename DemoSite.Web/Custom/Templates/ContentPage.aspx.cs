﻿using CMS.DocumentEngine;
using CMS.UIControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoSite.Web.Custom.Templates
{
    public partial class ContentPage : TemplatePage
    {
        /// <summary>
        /// Page H1 Title
        /// </summary>
        protected string PageHeaderTitle
        {
            get
            {
                return DocumentContext.CurrentDocument.GetStringValue("MenuItemName", string.Empty);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}