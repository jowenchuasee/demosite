﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Custom/Masterpages/Default.Master" AutoEventWireup="true" CodeBehind="ContentPage.aspx.cs" Inherits="DemoSite.Web.Custom.Templates.ContentPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="holderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="holderMain" runat="server">
    <h1><%=PageHeaderTitle %></h1>
    <cms:CMSWebPartZone ID="zoneBody" runat="server" />
</asp:Content>
