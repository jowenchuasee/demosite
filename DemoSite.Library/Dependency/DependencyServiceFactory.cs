﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using DemoSite.Library.Utilities;
using DemoSite.Library.Services;
using DemoMVC.Domain.Repository;

namespace DemoSite.Library.Dependency
{
    public class DependencyServiceFactory<T>
    {
        UnityContainer _container;

        public DependencyServiceFactory()
        {
            _container = GlobalUnityContainer.Container;
        }

        
        public T GetService()
        {   
            return _container.Resolve<T>();
        }
    }
}
