﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSite.Library.Dependency
{
    public class GlobalUnityContainer
    {
        private static UnityContainer container = null;
        private static readonly object padlock = new object();
        
        public static UnityContainer Container
        {
            get
            {
                lock (padlock)
                {
                    if (container == null)
                    {
                        container = new UnityContainer();
                    }
                    return container;
                }
            }
        }
    }
}
