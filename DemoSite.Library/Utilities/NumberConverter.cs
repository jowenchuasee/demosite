﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSite.Library.Utilities
{
    /// <summary>
    /// Class to convert 3 digit or less numbers
    /// </summary>
    public class NumberConverter : INumberConverter
    {
        private Dictionary<int, string> mWordDictionary;

        public NumberConverter()
        {
            InitDictionary();
        }

        /// <summary>
        /// initialise dictionary for words used in numbers
        /// </summary>
        private void InitDictionary()
        {
            mWordDictionary = new Dictionary<int, string>();
            mWordDictionary.Add(1, "one");
            mWordDictionary.Add(2, "two");
            mWordDictionary.Add(3, "three");
            mWordDictionary.Add(4, "four");
            mWordDictionary.Add(5, "five");
            mWordDictionary.Add(6, "six");
            mWordDictionary.Add(7, "seven");
            mWordDictionary.Add(8, "eight");
            mWordDictionary.Add(9, "nine");
            mWordDictionary.Add(10, "ten");
            mWordDictionary.Add(11, "eleven");
            mWordDictionary.Add(12, "twelve");
            mWordDictionary.Add(13, "thirteen");
            mWordDictionary.Add(14, "fourteen");
            mWordDictionary.Add(15, "fifteen");
            mWordDictionary.Add(16, "sixteen");
            mWordDictionary.Add(17, "seventeen");
            mWordDictionary.Add(18, "eighteen");
            mWordDictionary.Add(19, "nineteen");
            mWordDictionary.Add(20, "twenty");
            mWordDictionary.Add(30, "thirty");
            mWordDictionary.Add(40, "forty");
            mWordDictionary.Add(50, "fifty");
            mWordDictionary.Add(60, "sixty");
            mWordDictionary.Add(70, "seventy");
            mWordDictionary.Add(80, "eighty");
            mWordDictionary.Add(90, "ninety");

        }

        /// <summary>
        /// Method to convert 3 digit or less numbers
        /// </summary>
        /// <param name="paramNumberString">number in string format</param>
        /// <returns>number in words</returns>
        public string ConvertNumber(string paramNumberString)
        {
            string numberString = paramNumberString;
            string words = string.Empty;

            int i = 0;
            if (string.IsNullOrEmpty(numberString) || !int.TryParse(numberString, out i))
            {
                return string.Empty;
            }

            //hundreds
            if (numberString.Length == 3)
            {
                string value = string.Empty;
                int num = Convert.ToInt16(numberString.Substring(0, 1));
                mWordDictionary.TryGetValue(num, out value);

                if (!string.IsNullOrEmpty(value))
                {
                    words = string.Format("{0} hundred", value);
                }

                numberString = numberString.Substring(1);
            }

            string dash = string.Empty;

            //tens
            if (numberString.Length == 2)
            {
                string value = string.Empty;
                int tens = Convert.ToInt16(numberString.Substring(0, 1));

                if (tens > 0)
                {
                    //10-19
                    if (tens == 1)
                    {
                        tens = Convert.ToInt16(numberString);
                        numberString = string.Empty;
                    }
                    else
                    {
                        tens = tens * 10;
                        numberString = numberString.Substring(1);

                        if (numberString != "0")
                        {
                            dash = "-";
                        }
                    }
                    mWordDictionary.TryGetValue(tens, out value);

                    if (!string.IsNullOrEmpty(value))
                    {
                        if (!string.IsNullOrEmpty(words))
                        {
                            words += " ";
                        }
                        words += value;
                    }

                }
                else
                {
                    if (paramNumberString.Length == 3)
                    {
                        dash = " ";
                    }

                    numberString = numberString.Substring(1);
                }
            }

            //ones
            if (numberString.Length == 1)
            {
                string value = string.Empty;
                int num = Convert.ToInt16(numberString.Substring(0, 1));
                mWordDictionary.TryGetValue(num, out value);

                if (!string.IsNullOrEmpty(value))
                {
                    words += string.Format("{0}{1}", dash, value);
                }
            }

            return words;
        }
    }
}
