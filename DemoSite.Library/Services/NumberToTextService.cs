﻿using DemoSite.Library.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSite.Library.Services
{
    public class NumberToTextService : INumberToTextService
    {
        private INumberConverter mNumberConverter;

        public NumberToTextService(INumberConverter numberconverter)
        {
            mNumberConverter = numberconverter;
        }

        /// <summary>
        /// Get words after hundred
        /// </summary>
        /// <param name="index">which part of number</param>
        /// <returns></returns>
        private string GetGroupWord(int index)
        {
            switch (index)
            {
                case 2:
                    return " thousand";
                case 3:
                    return " million";
                case 4:
                    return " billion";
            }

            return string.Empty;
        }

        /// <summary>
        /// Recursive function to get words by hundreds or 3 digits
        /// </summary>
        /// <param name="dollar">number in string format</param>
        /// <param name="startingIndexToConvert">index in numeric string</param>
        /// <param name="iteration"></param>
        /// <returns>number in words</returns>
        private string GetWords(string dollar, int startingIndexToConvert, int iteration)
        {
            if (iteration == 1)
            {
                if (dollar.Length <= 3)
                {
                    startingIndexToConvert = 0;
                }
                else
                {
                    startingIndexToConvert = dollar.Length - 3;
                }
            }

            string textToConvert = string.Empty;

            if (dollar.Length < 3)
            {
                textToConvert = dollar;
            }
            else
            {
                int length = 3;
                if (startingIndexToConvert == 0)
                {
                    length = dollar.Length % 3;

                    if (length == 0)
                    {
                        length = 3;
                    }
                }

                textToConvert = dollar.Substring(startingIndexToConvert, length);
            }

            string space = string.Empty;
            if (startingIndexToConvert > 0)
            {
                space = " ";
            }
            string words = mNumberConverter.ConvertNumber(textToConvert);

            if (!string.IsNullOrEmpty(words))
            {
                words = space + words + GetGroupWord(iteration);
            }

            if (startingIndexToConvert == 0)
            {
                return words;
            }
            startingIndexToConvert = startingIndexToConvert - 3;

            if (startingIndexToConvert < 0)
            {
                startingIndexToConvert = 0;
            }
            iteration++;
            return GetWords(dollar, startingIndexToConvert, iteration) + words;
        }


        /// <summary>
        /// check if S is needed for dollar and cent
        /// </summary>
        /// <param name="amountWords"></param>
        /// <returns>"s" if word is not "one", otherwise empty</returns>
        private string addS(string amountWords)
        {
            return (amountWords == "one") ? string.Empty : "s";
        }

        /// <summary>
        /// Main method to convert decimal to words
        /// </summary>
        /// <param name="amount">number to convert to words</param>
        /// <returns>number in string format</returns>
        public string GenerateText(double amount)
        {
            string amountText = amount.ToString();
            string output = string.Empty;

            string[] arr = amountText.Split('.');

            if (arr.Length == 1) //no decimal
            {
                string dollarWords = GetWords(amountText, 0, 1);
                output = dollarWords + " dollar" + addS(dollarWords);
            }
            else
            {
                //Get words for whole number/dollar part
                string dollarWords = GetWords(arr[0], -1, 1);

                if (!string.IsNullOrEmpty(dollarWords))
                {
                    dollarWords += " dollar" + addS(dollarWords);
                }

                //Get words for decimal/cents part
                string centsWords = GetWords(arr[1], -1, 1);

                if (!string.IsNullOrEmpty(centsWords))
                {
                    if (!string.IsNullOrEmpty(dollarWords))
                    {
                        centsWords = string.Format(" and {0} cent{1}", centsWords, addS(centsWords));
                    }
                    else
                    {
                        centsWords = string.Format("{0} cent{1}", centsWords, addS(centsWords));
                    }
                }

                output = string.Format("{0}{1}", dollarWords, centsWords);
            }

            return output;
        }
    }
}
