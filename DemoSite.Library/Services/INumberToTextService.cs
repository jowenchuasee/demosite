﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSite.Library.Services
{
    public interface INumberToTextService
    {
        string GenerateText(double amount);
    }
}
