﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DemoSite.Library.Utilities;

namespace DemoSite.UnitTest
{
    [TestClass]
    public class NumberConverterTest
    {
        [TestMethod]
        public void Hundred_Is300()
        {
            var numberConverter = new NumberConverter();

            Assert.AreEqual(numberConverter.ConvertNumber("300"), "three hundred");
        }

        [TestMethod]
        public void OneDigit_Is1()
        {
            var numberConverter = new NumberConverter();

            Assert.AreEqual(numberConverter.ConvertNumber("1"), "one");
        }

        [TestMethod]
        public void ThreeDigit_Is111()
        {
            var numberConverter = new NumberConverter();

            Assert.AreEqual(numberConverter.ConvertNumber("111"), "one hundred eleven");
        }

        [TestMethod]
        public void TreeDigitZeroTens_Is801()
        {
            var numberConverter = new NumberConverter();

            Assert.AreEqual(numberConverter.ConvertNumber("801"), "eight hundred one");
        }

        [TestMethod]
        public void ThreeDigit_Is742()
        {
            var numberConverter = new NumberConverter();

            Assert.AreEqual(numberConverter.ConvertNumber("742"), "seven hundred forty-two");
        }

        [TestMethod]
        public void TwoDigit_Is85()
        {
            var numberConverter = new NumberConverter();

            Assert.AreEqual(numberConverter.ConvertNumber("85"), "eighty-five");
        }

        [TestMethod]
        public void NoOnes_Is370()
        {
            var numberConverter = new NumberConverter();

            Assert.AreEqual(numberConverter.ConvertNumber("370"), "three hundred seventy");
        }

        [TestMethod]
        public void IsBlank()
        {
            var numberConverter = new NumberConverter();

            Assert.AreEqual(numberConverter.ConvertNumber("0"), "");
        }
    }
}
